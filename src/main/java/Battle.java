import java.util.ArrayList;

public class Battle extends GameElement {
    private ArrayList<Enemy> enemies;

    public Battle(int id) {
        super(id, "BATTLE");
        this.enemies = new ArrayList<Enemy>();
    }

    public void addEnemy(Enemy enemy) {
        enemies.add(enemy);
    }

    @Override
    public void printElement() {
        super.printElement();
        for (int i = 0;i < this.enemies.size();i++) {
            this.enemies.get(i).printCharacter();
        }
    }
}