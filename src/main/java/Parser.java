import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class Parser {
    private Document doc;

    public Parser(String file) {
        this.doc = parseDocument(file);
    }

    public Document getDocument() {
        return this.doc;
    }

    public ArrayList<GameElement> parseSceneIntoState() {
        ArrayList<GameElement> state = new ArrayList<GameElement>();
        NodeList nList = this.doc.getElementsByTagName("Scene").item(0).getChildNodes();

        for (int i = 0;i < nList.getLength();i++) {
            switch(nList.item(i).getNodeName()) {
                case "Dialog":
                    state.add(parseDialog((Element) nList.item(i)));
                    break;
                case "Battle":
                    state.add(parseBattle((Element) nList.item(i)));
                    break;
            }
        }
        return state;
    }

    private static Dialog parseDialog(Element element) {
        Dialog eDialog = new Dialog(
            Integer.parseInt(element.getAttributes().getNamedItem("id").getTextContent()),
            element.getElementsByTagName("Name").item(0).getTextContent(),
            element.getElementsByTagName("Image").item(0).getTextContent(),
            element.getElementsByTagName("Text").item(0).getTextContent()
        );
        return eDialog;
    }

    private static Battle parseBattle(Element element) {
        Battle eBattle = new Battle(
            Integer.parseInt(element.getAttributes().getNamedItem("id").getTextContent())
        );
        NodeList enemyList = element.getElementsByTagName("Enemy");
        for (int j = 0;j < enemyList.getLength();j++) {
            Enemy enemy = new Enemy(
                Integer.parseInt(enemyList.item(j).getAttributes().getNamedItem("id").getTextContent()),
                enemyList.item(j).getAttributes().getNamedItem("name").getTextContent(),
                Integer.parseInt(enemyList.item(j).getAttributes().getNamedItem("pv").getTextContent()),
                Integer.parseInt(enemyList.item(j).getAttributes().getNamedItem("pm").getTextContent()),
                Integer.parseInt(enemyList.item(j).getAttributes().getNamedItem("attack").getTextContent()),
                Integer.parseInt(enemyList.item(j).getAttributes().getNamedItem("defense").getTextContent())
            );
            eBattle.addEnemy(enemy);
        }
        return eBattle;
    }

    private static Document parseDocument(String fileName) {
        try {
            File inputFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            return doc;
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
}