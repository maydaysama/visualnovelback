import java.util.ArrayList;

public class GameState {
    ArrayList<GameElement> state;

    public GameState() {
    }

    public void setState(ArrayList<GameElement> state) {
        this.state = state;
    }

    public ArrayList<GameElement> getState() {
        return this.state;
    }
}