import java.io.UnsupportedEncodingException;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;
import com.corundumstudio.socketio.listener.DataListener;

public class SocketWrapper {

    private Configuration config = new Configuration();
    private SocketIOServer server;

    public SocketWrapper(String domain, int port) {
        config.setHostname(domain);
        config.setPort(port);
        server = new SocketIOServer(this.config);

        server.addEventListener("msg", byte[].class, new DataListener<byte[]>() {
            @Override
            public void onData(SocketIOClient client, byte[] data, AckRequest ackRequest) {
                System.out.println("TEST");
                client.sendEvent("msg", "d");
            }
        });
    }
    public void listen() throws InterruptedException, UnsupportedEncodingException {
        this.server.start();
        Thread.sleep(Integer.MAX_VALUE);
        this.server.stop();
    };
}