public class Server {
    public static void main(String[] args) {
        Parser doc = new Parser("input.xml");
        GameState gameState = new GameState();

        gameState.setState(doc.parseSceneIntoState());
        for (int i = 0;i < gameState.getState().size();i++) {
            gameState.getState().get(i).printElement();
        }
    }
}