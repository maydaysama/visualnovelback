public class Dialog extends GameElement {
    private String text;
    private String name;
    private String image;

    public Dialog(int id, String name, String image, String text) {
        super(id, "DIALOG");
        this.text = text.trim();
        this.name = name.trim();
        this.image = image.trim();
    }

    public String getText() {
        return this.text;
    }

    public String getName() {
        return this.name;
    }

    public String getImage() {
        return this.image;
    }

    public void setText(String text) {
        this.text = text.trim();
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public void setImage(String image) {
        this.image = image.trim();
    }

    @Override
    public void printElement() {
        super.printElement();
        System.out.println("\tName: " + this.name);
        System.out.println("\tImage: " + this.image);
        System.out.println("\tText: " + this.text);
    }
}