public abstract class GameElement {
    private int id;
    private String mode;

    public GameElement(int id, String mode) {
        this.id = id;
        this.mode = mode;
    }

    public int getId() {
        return this.id;
    }

    public String getMode() {
        return this.mode;
    }

    public void printElement() {
        System.out.println(this.mode + " id: " + this.id);
    }
}