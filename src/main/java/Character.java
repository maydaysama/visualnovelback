public abstract class Character {
    private int id;
    private String name;
    private int pv;
    private int pm;
    private int attack;
    private int defense;

    public Character(int id, String name, int pv, int pm, int attack, int defense) {
        this.id = id;
        this.name = name;
        this.pv = pv;
        this.pm = pm;
        this.attack = attack;
        this.defense = defense;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getPV() {
        return this.pv;
    }

    public int getPM() {
        return this.pm;
    }

    public int getAttack() {
        return this.attack;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPV(int pv) {
        this.pv = pv;
    }

    public void setPM(int pm) {
        this.pm = pm;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void printCharacter() {
        String info = "\tid: "+ this.getId()
        + " -  name: " + this.getName()
        + " -  PV: " + this.getPV()
        + " -  PM: " + this.getPM()
        + " - attack: " + this.getAttack()
        + " - defense: " + this.getDefense();
        System.out.println(info);
    }
}